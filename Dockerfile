#our base node
#FROM node:12.6.0
#update version node
FROM mhart/alpine-node:8

#update bash
RUN apk add --no-cache bash tzdata

#update timezone
RUN ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime

#create app dir
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# If you are building your code for production
RUN npm install --only=production
# RUN npm install

# Bundle app source
COPY . .

#tell the port number the container should expose
EXPOSE 4000

#run the application
CMD ["npm", "start"]
