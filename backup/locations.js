import path from "path";
module.exports = app => {
    const Locations = app.db.locations;
    // let cache = require(path.join(__dirname, '../cache/memcache'))(30);
    let cache = app.cache.redis;

    /**
     * @api {get} /locations Get all asset location
     * @apiGroup Locations
     * @apiVersion 1.0.0
     * @apiName Get all asset location
     * @apiDescription Get all asset location
     * @apiSuccess {INTEGER} id id asset
     * @apiSuccess {STRING} lokasi lokasi asset
     * @apiSuccess {STRING} kota kota asset
     * @apiSuccess {STRING} status status asset
     * @apiSuccess {STRING} lat lat asset
     * @apiSuccess {STRING} long long asset
     * @apiSuccessExample {json} Success:
     *    HTTP/1.1 200 OK
     *    {
     *      {
     *           "id": 1,
     *           "lokasi": "Kwitang  Pintu Masuk",
     *           "kota": "Jakarta Pusat",
     *           "status": "Live",
     *           "lat": null,
     *           "long": null
     *       },
     *       {
     *           "id": 2,
     *           "lokasi": "Kwitang  ATM",
     *           "kota": "Jakarta Pusat",
     *           "status": "Live",
     *          "lat": null,
     *           "long": null
     *       }
     *    }
     */
    app.get("/locations", app.libs.auth.authenticate(), cache, (req, res) => {
        Locations.findAll({
            attributes: ['id', 'lokasi', 'kota', 'status', 'lat', 'long']
        })
            .then(result => {
                if(result)
                    res.json(result);
                else
                    res.sendStatus(404);
            })
            .catch(error => {res.status(412).json({msg : error.message})});
    });

    /**
     * @api {get} /locations/:kota Get all asset location by kota
     * @apiGroup Locations
     * @apiVersion 1.0.0
     * @apiName Get all asset location by kota
     * @apiDescription Get all asset location by kota
     * @apiParam {STRING} kota kota asset
     * @apiParamExample {json} Request:
     * {
     *      "kota": "SBY"
     * }
     * @apiSuccess {INTEGER} id id asset
     * @apiSuccess {STRING} lokasi lokasi asset
     * @apiSuccess {STRING} kota kota asset
     * @apiSuccess {STRING} status status asset
     * @apiSuccess {STRING} lat lat asset
     * @apiSuccess {STRING} long long asset
     * @apiSuccessExample {json} Success:
     *    HTTP/1.1 200 OK
     *    {
     *      {
     *           "id": 1,
     *           "lokasi": "Kwitang  Pintu Masuk",
     *           "kota": "Jakarta Pusat",
     *           "status": "Live",
     *           "lat": null,
     *           "long": null
     *       },
     *       {
     *           "id": 2,
     *           "lokasi": "Kwitang  ATM",
     *           "kota": "Jakarta Pusat",
     *           "status": "Live",
     *          "lat": null,
     *           "long": null
     *       }
     *    }
     */
    app.get("/locations/:kota", app.libs.auth.authenticate(), cache, (req, res) => {
        Locations.findAll({
            attributes: ['id', 'lokasi', 'kota', 'status', 'lat', 'long'],
            where: {
                kota: req.params.kota
            }
        })
            .then(result => {
                if(result)
                    res.json(result);
                else
                    res.sendStatus(404);
            })
            .catch(error => {res.status(412).json({msg : error.message})});
    });
};