import jwt from "jwt-simple";
//route for token
module.exports = app => {
    const cfg = app.libs.config;
    const Users = app.db.users;

    app.post("/token", (req, res) => {
        if(req.body.user)
        {
            const user = req.body.user;
            // const password = req.body.password;
            Users.findOne({where: { username: user }})
                .then(user => {
                    if(user){
                        const payload = {id: user.id};
                        res.json({
                            token: jwt.encode(payload, cfg.jwtSecret)
                        });
                    }else{
                        res.sendStatus(401);
                    }
                })
                .catch(error => res.sendStatus(401));
        }
        else
        {
            res.sendStatus(401);
        }
    });
};