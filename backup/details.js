import path from "path";
module.exports = app => {
    const Locations = app.db.locations;
    // let cache = require(path.join(__dirname, '../cache/memcache'))(30);
    let cache = app.cache.redis;
    /**
     * @api {get} /details Get all details asset
     * @apiGroup Details
     * @apiVersion 1.0.0
     * @apiName Get all details asset
     * @apiDescription Get all detail asset
     * @apiSuccess {INTEGER} id id lokasi
     * @apiSuccess {STRING} jenis jenis asset
     * @apiSuccess {STRING} lokasi lokasi asset
     * @apiSuccess {STRING} status status asset
     * @apiSuccess {STRING} ip ip location asset
     * @apiSuccessExample {json} Success:
     *    HTTP/1.1 200 OK
     *    {
     *      {
     *           "id": 1,
     *           "jenis": "Pintu Masuk",
     *           "lokasi": "Kwitang  Pintu Masuk",
     *           "status": "Live",
     *           "ip": "10.10.32.82"
     *       },
     *       {
     *           "id": 2,
     *          "jenis": "ATM",
     *           "lokasi": "Kwitang  ATM",
     *           "status": "Live",
     *           "ip": "10.10.32.83"
     *       }
     *    }
     */
    app.get("/details", app.libs.auth.authenticate(), cache, (req, res) => {
        Locations.findAll({
                attributes: ['id', 'jenis', 'lokasi', 'status', 'ip']
            })
            .then(result => {
                if(result)
                    res.json(result);
                else
                    res.sendStatus(404);
            })
            .catch(error => {res.status(412).json({msg : error.message})});
    });

    /**
     * @api {get} /details/:jenis Get all details asset by jenis
     * @apiGroup Details
     * @apiVersion 1.0.0
     * @apiName Get all details asset by jenis
     * @apiDescription Get all details asset by jenis
     * @apiParam {STRING} jenis jenis asset
     * @apiParamExample {json} Request:
     * {
     *      "jenis": "ATM"
     * }
     * @apiSuccess {INTEGER} id id lokasi
     * @apiSuccess {STRING} jenis jenis asset
     * @apiSuccess {STRING} lokasi lokasi asset
     * @apiSuccess {STRING} status status asset
     * @apiSuccess {STRING} ip ip location asset
     * @apiSuccessExample {json} Success:
     *    HTTP/1.1 200 OK
     *    {
     *      {
     *           "id": 1,
     *           "jenis": "ATM",
     *           "lokasi": "Kwitan  Pintu Masuk ATM",
     *           "status": "Live",
     *           "ip": "10.10.32.82"
     *       },
     *       {
     *           "id": 2,
     *          "jenis": "ATM",
     *           "lokasi": "Kwitang  ATM",
     *           "status": "Live",
     *           "ip": "10.10.32.83"
     *       }
     *    }
     */
    app.get("/details/:jenis", app.libs.auth.authenticate(), cache, (req, res) => {
        Locations.findAll({ 
                attributes: ['id', 'jenis', 'lokasi', 'status', 'ip'],
                where: { jenis: req.params.jenis }
            })
            .then(result => {
                if(result)
                    res.json(result);
                else
                    res.sendStatus(404);
            })
            .catch(error => {res.status(412).json({msg : error.message})});
    });

    /**
     * @api {get} /details/:jenis/:kota Get all details asset by jenis, kota
     * @apiGroup Details
     * @apiVersion 1.0.0
     * @apiName Get all details asset by jenis, kota
     * @apiDescription Get all details asset by jenis, kota
     * @apiParam {STRING} jenis jenis asset
     * @apiParam {STRING} kota kota asset
     * @apiParamExample {json} Request:
     * {
     *      "jenis": "ATM",
     *      "kota": "SBY"
     * }
     * @apiSuccess {INTEGER} id id lokasi
     * @apiSuccess {STRING} jenis jenis asset
     * @apiSuccess {STRING} lokasi lokasi asset
     * @apiSuccess {STRING} status status asset
     * @apiSuccess {STRING} ip ip location asset
     * @apiSuccessExample {json} Success:
     *    HTTP/1.1 200 OK
     *    {
     *      {
     *           "id": 1,
     *           "jenis": "ATM",
     *           "lokasi": "Kwitan  Pintu Masuk ATM",
     *           "status": "Live",
     *           "ip": "10.10.32.82"
     *       },
     *       {
     *           "id": 2,
     *          "jenis": "ATM",
     *           "lokasi": "Kwitang  ATM",
     *           "status": "Live",
     *           "ip": "10.10.32.83"
     *       }
     *    }
     */
    app.get("/details/:jenis/:kota", app.libs.auth.authenticate(), cache, (req, res) => {
        Locations.findAll({ 
                attributes: ['id', 'jenis', 'lokasi', 'status', 'ip'],
                where: { 
                    jenis: req.params.jenis,
                    kota: req.params.kota
                 }
            })
            .then(result => {
                if(result)
                    res.json(result);
                else
                    res.sendStatus(404);
            })
            .catch(error => {res.status(412).json({msg : error.message})});
    });
}