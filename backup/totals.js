import path from "path";

module.exports = app => {
    const Locations = app.db.locations;
    const sequelize = app.db.Sequelize;
    let cache = app.cache.redis;
    // let cache = require(path.join(__dirname, '../cache/memcache'))(30);
    /**
     * @api {get} /totals Get sum of assets
     * @apiGroup Totals
     * @apiVersion 1.0.0
     * @apiName Get sum of assets
     * @apiDescription Get sum of assets
     * @apiSuccess {STRING} jenis jenis asset
     * @apiSuccess {STRING} total total asset
     * @apiSuccessExample {json} Success:
     *    HTTP/1.1 200 OK
     *    {
     *      {
     *           "jenis": "ATM",
     *           "total": 10
     *       },
     *       {
     *           "jenis": "Parkiran",
     *           "total": 10
     *       }
     *    }
     */
    app.get("/totals", app.libs.auth.authenticate(), cache, (req, res) => {
        Locations.findAll({ 
            attributes:["jenis", [sequelize.fn('COUNT', sequelize.col('jenis')), 'total']],
            group: ['jenis']  
        })
            .then(result => {
                if(result)
                    res.json(result);
                else
                    res.sendStatus(404);
            })
            .catch(error => {res.status(412).json({msg : error.message})});
    });

    /**
     * @api {get} /totals/:jenis Get sum of assets by jenis
     * @apiGroup Totals
     * @apiVersion 1.0.0
     * @apiName Get sum of assets by jenis
     * @apiDescription Get sum of assets by jenis
     * @apiParam {STRING} jenis jenis asset
     * @apiParamExample {json} Request:
     * {
     *      "jenis": "ATM"
     * }
     * @apiSuccess {STRING} jenis jenis asset
     * @apiSuccess {STRING} total total asset
     * @apiSuccessExample {json} Success:
     *    HTTP/1.1 200 OK
     *    {
     *      {
     *           "jenis": "ATM",
     *           "total": 10
     *       }
     *    }
     */
    app.get("/totals/:jenis", app.libs.auth.authenticate(), cache, (req, res) => {
        Locations.findAll({
            attributes:["jenis", [sequelize.fn('COUNT', sequelize.col('jenis')), 'total']],
            group: ['jenis'],
            where: {
                jenis: req.params.jenis
            }
        })
            .then(result => {
                if(result)
                    res.json(result);
                else
                    res.sendStatus(404);
            })
            .catch(error => {res.status(412).json({msg : error.message})});
    });

    /**
     * @api {get} /totals/:jenis/:kota/:kd_lokasi Get sum of assets by jenis, kota, kode lokasi
     * @apiGroup Totals
     * @apiVersion 1.0.0
     * @apiName Get sum of assets by jenis, kota, kode lokasi
     * @apiDescription Get sum of assets by jenis, kota, kode lokasi
     * @apiParam {STRING} jenis jenis asset
     * @apiParam {STRING} kota kota asset
     * @apiParam {STRING} kd_lokasi kode lokasi asset
     * @apiParamExample {json} Request:
     * {
     *      "jenis": "ATM",
     *      "kota": "SBY",
     *      "kd_lokasi": 22
     * }
     * @apiSuccess {STRING} jenis jenis asset
     * @apiSuccess {STRING} total total asset
     * @apiSuccessExample {json} Success:
     *    HTTP/1.1 200 OK
     *    {
     *      {
     *           "jenis": "ATM",
     *           "total": 10
     *       }
     *    }
     */
    app.get("/totals/:jenis/:kota/:kd_lokasi", app.libs.auth.authenticate(), cache, (req, res) => {
        Locations.findAll({
            attributes:["jenis", [sequelize.fn('COUNT', sequelize.col('jenis')), 'total']],
            group: ['jenis'],
            where: {
                jenis: req.params.jenis,
                kota: req.params.kota,
                kode_lokasi: req.params.kd_lokasi
            }
        })
            .then(result =>{
                if(result)
                    res.json(result);
                else
                    res.sendStatus(404);
            })
            .catch(error => {res.status(412).json({msg : error.message})});
    });
}