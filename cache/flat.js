import FlatCache from "flat-cache";
import path from "path";

let flatcache = null;

module.exports = (Filename) => {

    let cacheFile = FlatCache.load(Filename, (path.join(__dirname, 'content')));

    if(!flatcache){
        flatcache = (req, res, next) => {
            let key = '_express_' + req.originalUrl || req.url
            let cachedBody = cacheFile.getKey(key)
            if(cachedBody)
            {
                res.send(cachedBody);
            }
            else
            {
                res.sendResponse = res.send
                res.send = (body) => {
                    cacheFile.setKey(key, body)
                    cacheFile.save()
                    res.sendResponse(body)
                }
                next()
            }
        }
    }

    return flatcache;
}