import MemoryCache from "memory-cache";

module.exports = app => {
    
    const cfg = app.libs.config.cache;
    return (req, res, next) => {
        let key = cfg.key + req.originalUrl || req.url
        let cacheContent = MemoryCache.get(key);
        if(cacheContent)
        {
            res.send(cacheContent)
            return
        }
        else
        {
            res.sendResponse = res.send
            res.send = (body) => {
                MemoryCache.put(key, body, cfg.duration*1000)
                res.sendResponse(body)
            }
            next()
        }
    }
};