import redis from "redis";

let rediscache = null;

module.exports = app => {

    const cfg = app.libs.config.cache;
    const client = redis.createClient({ host: cfg.host, port: cfg.port });
    
    if(!rediscache)
    {
        rediscache = (req,res,next) => {
            let key = cfg.key + req.originalUrl || req.url
            console.log(key)
            client.get(key, function(err, reply){
                if(reply) {
                    res.send(JSON.parse(reply))
                }else {
                    res.sendResponse = res.send
                    res.send = (body) => {
                        client.set(key, JSON.stringify(body))
                        client.expire(key, cfg.duration)
                        res.sendResponse(body)
                    }
                    next()
                }
            })
        }
    }

    return rediscache;
}