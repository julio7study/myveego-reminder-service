import express from "express"; //web server
import consign from "consign"; //arrange module

const app = express();

//consign insert module into app
consign()
    .include("libs/config.js") //include config db
//    .then("cache/redis.js")
    .then("database") //create connect db
//    .then("libs/auth.js") //authentication
    .then("libs/middleware.js") //load middleware for setting
    .then("routes") //load routes
    .then("libs/boot.js") //load boot using setting from middleware
    .into(app);