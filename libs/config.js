module.exports = {
    auth: {
        jwtSession: { session: false},
        jwtSecret: "s3c12et"
    },
    noSql: {
        host: process.env.MONGO_HOST || "20.191.139.53",
        port: process.env.MONGO_PORT || 4935,
        dbadmin: process.env.MONGO_DB || "admin",
        database: process.env.MONGO_DB || "FLEET",
        user: process.env.MONGO_USER || "mongoadmin",
        pass: process.env.MONGO_PASS || "password123",
        url: process.env.MONGO_URL || null
    },
    cache: {
        host : process.env.CACHE_HOST || "192.168.56.101",
        port : process.env.CACHE_PORT || 9999,
        duration : process.env.CACHE_DURATION || 120, //cache duration
        key: process.env.KEY || "_framework_service_"
    },
    database : process.env.DB_NAME || "fleego",
    username : process.env.DB_USER ||"fleego",
    password : process.env.DB_PASS ||"Fl_3eG00_oY3",
    params: {
        dialect : "mysql", //sqlite,mysql,postgres & sqlserver for instanse db
        // replication: {
        //     read: [
        //       { host: '8.8.8.8', username: 'read-username', password: 'some-password' },
        //       { host: '9.9.9.9', username: 'another-username', password: null }
        //     ],
        //     write: { host: '1.1.1.1', username: 'write-username', password: 'any-password' }
        // },
        host : process.env.DB_HOST || "20.191.139.53",
        port : process.env.DB_PORT || "4335",
        pool: {
            max: 10,
            // min: 0,
            // acquire: 30000,
            idle: 10000
        },
        // storage : "belajar.sqlite",
        define : {
            underscored : true,
            timestamps : false,
            freezeTableName: true,
            defaultPrimaryKey: false
        },
        dialectOptions: {
            //useUTC: false, //for reading from database
            dateStrings: true,

            typeCast: function (field, next) { // for reading from database
                if (field.type === 'DATETIME') {
                    return field.string()
                }
                return next()
            },
        },
        timezone: '+07:00'
    }
};