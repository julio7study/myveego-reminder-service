//export module for setting app
import bodyParser from "body-parser";
import express from "express";
import compression from "compression";
import helmet from "helmet";
import cors from "cors";

module.exports = app => {

    app.set("json spaces", 4);
    app.set("port", 4100); //di server 4100

    //add helmet for secure express
    app.use(helmet());
    app.use(cors());

    //add compression
    app.use(compression());
    //delete route to all() function from every router and
    //remove from middleware
    // app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    // for authentication
    // app.use(app.libs.auth.initialize());
    app.use((req, res, next) => {
        delete req.body.id;
        next();
    });

    //see generate doc
    app.use(express.static("public"));
};