import passport from "passport";
import {Strategy, ExtractJwt} from "passport-jwt";

module.exports = app => {
    const Users = app.database.mysql.users;
    const cfg = app.libs.config.auth;
    
    const params = {
      secretOrKey: cfg.jwtSecret,
      jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt')
    };
    const strategy = new Strategy(params, (payload, done) => {
        Users.findById(payload.id)
          .then(user => {
                if (user) {
                    return done(null, {
                        id: user.id
                    });
                }
            return done(null, false);
          })
          .catch(error => done(error, null));
      });
    passport.use(strategy);

    return {
      initialize: () => {
        return passport.initialize();
      },
      authenticate: () => {
        return passport.authenticate("jwt", cfg.jwtSession);
      }
    };
  };