//create setting for run app
module.exports = app => {
    app.database.mysql.sequelize.sync().done(() => { //for sync to db before start server
        app.listen(app.get('port'), () => console.log(`api listening in port ${app.get('port')}`));
    });
};