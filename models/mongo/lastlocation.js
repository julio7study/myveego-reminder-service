module.exports = (Mongoose) => {

    const Lastlocation = new Mongoose.Schema({
        datetime: {
            type: String
        },
        address: {
            type: String
        },
        lat: {
            type: Number
        },
        lng: {
            type: Number
        },
        total_hour: {
            type: Number
        },
        total_distance: {
            type: Number
        },
        imei: {
            type: String
        }
    });

    return Lastlocation;
};