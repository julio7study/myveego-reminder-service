module.exports = (Mongoose) => {

    const Logs = new Mongoose.Schema({
        datetime: {
            type: String
        },
        id: {
            type: String
        },
        status: {
            type: String
        }
    });

    return Logs;
};