module.exports = (sequelize, DataType) => {
    const Users = sequelize.define("users", {
        id:{
            type: DataType.INTEGER,
            primaryKey: true
        },
        username:{
            type: DataType.STRING
        },
        addr:{
            type: DataType.STRING
        }
    });

    return Users;
}