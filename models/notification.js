module.exports = (sequelize, DataType) => {
    const Notification = sequelize.define("notif_list", {
        id:{
            type: DataType.INTEGER,
            primaryKey: true
        },
        vehicle_id:{
            type: DataType.INTEGER
        },
        notif_id:{
            type: DataType.INTEGER
        },
        notif_value:{
            type: DataType.STRING
        },
        notif_date:{
            type: DataType.DATE
        },
        read_status:{
            type: DataType.INTEGER
        }

    });

    return Notification;
}