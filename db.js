//for setting db
import Sequelize from "sequelize";
import path from "path";

let db = null;

module.exports = app => {
    if(!db){
        const config = app.libs.config;
        const sequelize = new Sequelize(config.database, 
            config.username, 
            config.password, 
            config.params);

        db = {};
        db.sequelize = sequelize;
        db.Sequelize = Sequelize;

        //Model
        // db.locations = require(path.join(__dirname, "models", "locations.js"))(sequelize, Sequelize);
        db.users = require(path.join(__dirname, "models", "users.js"))(sequelize, Sequelize);
        db.notification = require(path.join(__dirname, "models", "notification.js"))(sequelize, Sequelize);
    }

    return db;
};