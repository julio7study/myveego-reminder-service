import path from "path";
const geolib = require('geolib');

module.exports = app => {
    const Users = app.database.mysql.users;
    const NotifList = app.database.mysql.notification;
    const Db = app.database.mysql.sequelize;
    const Atmbeat = app.database.mongodb.lastlocation;
    const Lastlocation = app.database.mongodb.lastlocation;

    let arrayLastNotif={};
    let vehicleId = [];
    // arrayRecentDate['atmTes1']=new Date();
    // arrayRecentDate['atmTes2']=new Date();
//    arrayRecentDate['1234567890']=new Date();
    /**
     * @api {get} /users Get all users
     * @apiGroup Users
     * @apiVersion 1.0.0
     * @apiName Get all users
     * @apiDescription Get all users
     * @apiSuccess {INT} id user id
     * @apiSuccess {STRING} username username
     * @apiSuccess {STRING} addr user address
     * @apiSuccessExample {json} Success:
     *    HTTP/1.1 200 OK
     *    {
     *      {
     *           "id": "1",
     *           "username": administrator,
     *           "addr": ""
     *       },
     *       {
     *           "id": "17",
     *           "username": Operator1,
     *           "addr": "Sudirman"
     *       }
     *    }
     */
    app.get("/users",  (req, res) => {
        Users.findAll({
         limit:100
        })
            .then(result => {
                if(result)
                    res.json(result);
                else
                    res.sendStatus(404);
            })
            .catch(error => {res.status(412).json({msg : error.message})});
    });

    app.get("/test",  (req, res) => {
//        countDown();
        res.json({msg : 'tes countdown'});
    });

    app.post("/notifgate", (req, res) => {
//        console.log('data:');
  //      console.log(req.body);
        Db.query("select * from v_notif where imei_no = :imei AND notif_status=1", { replacements: { imei: req.body.imei }, types: Db.QueryTypes.SELECT})
            .then(result => {
                if(result) {
                    // console.log('result');
                    // console.log(result[0]);
                    if(result[0].length==0){
                        console.log("no notification configuration");
                        res.json("no notification configuration");

                    } else {
                        Db.query("select * from notif_list where vehicle_id = :vid AND read_status=0 order by id desc", {
                            replacements: {vid: result[0][0].vehicle_id},
                            types: Db.QueryTypes.SELECT
                        })
                            .then(result2 => {
                                // console.log('result2');
                                // console.log(result2[0]);
                                if (result2[0].length==0) {
                                    creating_new_notif(result, req, function(message){
                                        res.json(message);
                                    });
                                } else {
                                    processing_notif2(result, result2, req, function(message){
                                        res.json(message);
                                    });
                                }
                            })
                            .catch(error => {
                                console.log('Error: '+error.message);
                                res.status(412).json({msg: error.message})
                            });
                    }
                } else {
                    console.log('no config found');
                    res.sendStatus(404);
                }
            })
            .catch(error => {res.status(412).json({msg : error.message})});
    });

    app.post("/heartbeat", (req, res) => {
        arrayRecentDate[req.body.imei] = new Date(req.body.datetime);
        console.log(req.body.imei + ' ' + req.body.datetime);
        res.json('accepted '+req.body.imei+' at '+arrayRecentDate[req.body.imei]);
    });

    app.post("/direct", (req, res) => {
        Db.query("select * from vehicle where imei_no = :imei", { replacements: { imei: req.body.imei }, types: Db.QueryTypes.SELECT})
            .then(result => {
                if(result) {
                    const notes=req.body.type+' has occurred';
                    console.log('Pop up notif');
                    live_notif(result[0][0].user_id,result[0][0].plat_no, 'ALERT', notes);
                    res.json(result);
                }
                else {
                    res.sendStatus(404);
                }
            })
            .catch(error => {res.status(412).json({msg : error.message})});
    });

    app.post("/telegram", (req, res) => {
        Db.query("select * from vehicle where imei_no = :imei", { replacements: { imei: req.body.imei }, types: Db.QueryTypes.SELECT})
            .then(result => {
                if(result) {
                    const notes=req.body.type+' has occurred';
                    console.log('Direct Telegram');
                    live_notif(result[0][0].user_id,result[0][0].plat_no, 'ALERT', notes);
                    res.json(result);
                }
                else {
                    res.sendStatus(404);
                }
            })
            .catch(error => {res.status(412).json({msg : error.message})});
    });
    function notifType1(config, data){ //geo in
        console.log('check1 ',config.notif_name);
        let result = geofence(config, data);
        if(result){
            const notes='Geofence In: '+config.vehicle_name+' ('+config.plat_no+') is inside '+config.notif_name+' at '+data.datetime+'. Click here to open map: https://www.google.com/maps/search/?api=1&query='+data.lat+','+data.lng;
            console.log(notes);
            dispatch_notif(config, data, notes);
        }
        return result;
    }

    function notifType2(config, data){ //geo out
        console.log('check2 ',config.notif_name);
        let result = geofence(config, data);
        if(!result){
            const notes='Geofence Out: '+config.vehicle_name+' ('+config.plat_no+') is outside '+config.notif_name+' at '+data.datetime+'. Click here to open map: https://www.google.com/maps/search/?api=1&query='+data.lat+','+data.lng;
            console.log(notes);
            dispatch_notif(config, data, notes);
        }
        return !result;
//        console.log(geofence(config, data));
    }

    function notifType3(config, data){ //battery
        console.log('check3 ',config.notif_name);
        return true;
    }

    function notifType4(config, data){ //air filter
        console.log('check4 ',config.notif_name);
        return true;
    }

    function notifType5(config, data){ //Long Idle
        console.log('check5 ',config.notif_name);
        console.log('type: ',data.type);

        if(data.type=='Long Idle'){
            const notes='Long Idle: '+config.vehicle_name+' ('+config.plat_no+') at '+data.datetime+'. Click here to open map: https://www.google.com/maps/search/?api=1&query='+data.lat+','+data.lng;
            console.log(notes);
            dispatch_notif(config, data, notes);

        }
        return true;
    }

    function geofence(config, data){
        console.log('shape: '+config.shape);
        if(config.shape=='polygon'){
            return checkPolygon(data.lat, data.lng, JSON.parse(config.value));
        } else if (config.shape=='circle') {
            var center_shape = config.center_shape;
            var center_coord = center_shape.split(',');
            return checkCircle(data.lat, data.lng, JSON.parse(config.value), center_coord[0], center_coord[1], parseInt(config.radius));
        } else {
            return false;
        }
    }

    function checkPolygon(lat, lng, polygon){
        console.log('check polygon: '+lat+' '+lng+' '+polygon);
        var result = geolib.isPointInPolygon({latitude: lat, longitude: lng}, polygon);
        console.log('result: '+result);
        return result;
    }

    function checkCircle(lat, lng, center, latCenter, lngCenter, radius){
        console.log('check circle: '+lat+' '+lng+' '+latCenter+' '+lngCenter+" "+radius);
        var result =  geolib.isPointWithinRadius({latitude: lat, longitude: lng}, {latitude: latCenter, longitude: lngCenter}, radius);
        console.log('result: '+result);
        return result;
    }

    function creating_new_notif(result, req, cb){
        let message = [];

        result[0].forEach(function (element) {
            let fname = "notifType" + element.notif_type;
            let result = eval(fname)(element, req.body);

            let msg='';
            if(result){
                msg = "create new notif "+element.notif_name;
            } else {
                msg = "logic result is false => "+element.notif_name;
            }
            console.log(msg);
            message.push(msg);
        });

        cb(message);
    }

    function processing_notif(result, result2, req, cb){
        let newResult = [];
        let message = [];

        result[0].forEach(function (element, key){
            // if(element!=='undefined'){
            let itemFound = false;
            result2[0].forEach(function (element2, key2){
                // if(element2.notif_id==result[0][key].id){
                if(element2.notif_id==element.id){
                    // console.log("trash notif "+element.notif_name);
                    // delete result[0][key];
                    itemFound = true;
                }
            });
            if (!itemFound) newResult.push(element);
            // }
        });
        // console.log('new result');
        // console.log(newResult);
        // result[0].forEach(function (element){
        newResult.forEach(function (element){
            if(element!=='undefined'){
                let fname = "notifType" + element.notif_type;
                let result = eval(fname)(element, req.body);
                let msg='';
                if(result){
                    msg = "notif red, create new notif "+element.notif_name;
                } else {
                    msg = "notif red, logic result is false => "+element.notif_name;
                }
                console.log(msg);
                message.push(msg);
            }
        });

        cb(message);
    }

    function processing_notif2(result, result2, req, cb){
        let imei = req.body.imei;
        let notif_config = result;
        let notif_list = result2;

        let message=[];
        let msg='';
        if(arrayLastNotif.hasOwnProperty(imei)){
            console.log('telegram: '+notif_config[0][0].des_telegram);
            if(arrayLastNotif[imei]==notif_list[0][0].notif_id){
                msg = imei+' has same notification id: '+notif_list[0][0].notif_id;
                message.push(msg);
                console.log(msg);
            } else {
                notif_config[0].forEach(function (element) {
                    let fname = "notifType" + element.notif_type;
                    let result = eval(fname)(element, req.body);
                    if(result){
                        arrayLastNotif[imei]=element.id;
                        msg = "create new notif "+element.notif_name;
                    } else {
                        msg = "logic result is false => "+element.notif_name;
                    }
                    console.log(msg);
                    message.push(msg);
                });

            }
        } else {
            arrayLastNotif[imei]=notif_list[0][0].notif_id;
            msg = imei+' do init notification id: '+notif_list[0][0].notif_id;
            console.log(msg);
            message.push(msg);
        }

        cb(message);

    }
    function dispatch_notif(config, data, notes){
        console.log('telegram: '+config.des_telegram);
        if(config.is_app==1){
            live_notif(config.user_id, config.plat_no, data.type, notes);
        }
        if(config.is_email==1){
            send_email(config.notif_name, config.des_email, config.imei_no, data.lat, data.lng, notes, data.datetime);
        }
        if(config.is_sms==1){
        }
        if(config.is_telegram==1){
            send_telegram(notes, config, config.des_telegram);
        }

        insert_notiflist(config, data);
    }

    function insert_notiflist(config, data){
        let value = {"lat":data.lat,"lng":data.lng,"location":"undefined"};
        Db.query("insert into notif_list (vehicle_id, notif_id, notif_value, notif_date, read_status) values (:vehicle_id, :n_id, :n_value, :n_date, :read)", { replacements: { vehicle_id: config.vehicle_id , n_id: config.id, n_value: JSON.stringify(value), n_date: data.datetime, read:0  }, types: Db.QueryTypes.INSERT})
            .then(result => {
                if(result) {
                    console.log(result)
                }
                else {
                    console.log('Fail query?');
                }
            })
            .catch(error => {console.log(error.message)});
    }

    function send_email(type, email, nama, lat, lng, value, date){
        const request = require("request");

        request.post(
            `https://tracking.myveego.id/notification/api/email`,
            {
                json: {
                    "type" : type,
                    "email": email,
                    "nama" : nama,
                    "lat" : lat,
                    "lng" : lng,
                    "value" : value,
                    "date" : date
                }
            },
            function (error, response, body) {
                if (error) throw new Error(error);

                console.log(body);
            }
        );

    }

    function live_notif(user_id, platnomor, jenis, notes){
        const request = require("request");

        const options = { method: 'POST',
            url: 'https://dev.myveego.id:5001/api/notif',
            headers:
                { 'Postman-Token': '7227a753-ed4b-4011-a67b-863e9b39d5ce',
                    'Cache-Control': 'no-cache',
                    'Content-Type': 'application/x-www-form-urlencoded' },
            form:
                { user_id: user_id,
                    platnomor: platnomor,
                    jenis: 'ALERT',
                    notes: notes } };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            console.log(body);
        });
    }

    function insertNotification(data){
    }

    function send_telegram(notes, config, group_id='-1001165938179'){
//        console.log('telegram');
        const request = require("request");
        request.post(
            `http://dev.myveego.id:10000/api/telegram/sent-message`,
            {
                json: {
                             id_group: group_id,
                                text: notes
                }
            },
            function (error, response, body) {
                if (error) throw new Error(error);

                console.log(body);
            }
        );

        // const options = { method: 'POST',
        //     url: 'http://test.myveego.id:10000/api/telegram/send-message',
        //     headers:
        //         { 'cache-control': 'no-cache',
        //             Connection: 'keep-alive',
        //             'Accept-Encoding': 'gzip, deflate',
        //             Host: 'test.myveego.id:10000',
        //             'Postman-Token': 'cd70e7d0-fa39-4f41-88f5-405be7100feb,51c404d5-a826-45c8-aefb-e489c316f590',
        //             'Cache-Control': 'no-cache',
        //             Accept: '*/*',
        //             'User-Agent': 'PostmanRuntime/7.15.2',
        //             'Content-Type': 'application/x-www-form-urlencoded',
        //             'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' },
        //     formData:
        //         { id_group: '-1001165938179',
        //             text: notes } };
        //
        // request(options, function (error, response, body) {
        //     if (error) throw new Error(error);
        //
        //     console.log(body);
        // });
    }

    function insertBeat(datetime, id, status){
        let data = {
            datetime: datetime,
            id: id,
            status: status
        };
        new Atmbeat(data).save()
            .then(result => {
                if (result) {
                    console.log(result);
                }
                else {
                    console.log(id+': something went wrong with mongo');
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    function getATM(){//ambil list imei semua ATM yg terdaftar

    }
    getATM();
    
    setInterval(function () {
        getVreminderResume();
    }, 3600000);

    function getVreminderResume(){
        
        //get all dari tabel vreminder resume
        Db.query("select * from v_reminderresume", {types: Db.QueryTypes.SELECT})
            .then(result => {

                let vehicleId = [];
        //daftarkan semua vehicle id ke array, agar tidak double
                result[0].forEach(function (element2, key2){
                    arrayLastNotif[element2.vehicle_id+'_'+element2.type]=true
                    if(vehicleId[element2.imei]){
                    } else {
                    vehicleId[element2.imei]=true
                    }
//                    console.log(element2)
                });
                
                let arrayLen = vehicleId.length;
                Object.keys(vehicleId).forEach(function(key) {
//                    Lastlocation.find({ imei: key}, function (err, docs) {
  //                     vehicleId[key]=docs
    //                });
                }); 
                
                result[0].forEach(function (element2, key2){
                  let notes='your '+element2.reminder_name+' is overdue';
                  if(element2.status==1){
                    if(element2.is_app==1){
                    console.log('app||'+element2.plat_no+'||'+element2.type+'||'+notes)
                              live_notif(element2.user_id, element2.plat_no, element2.type, notes);
                    }
                    if(element2.is_email==1){
                    console.log('email||'+element2.reminder_type_name+'||'+element2.des_email+'||'+element2.imei+'||'+notes)
                    console.log(vehicleId[element2.imei]);
  //                  console.log(vehicleId[element2.imei][0].lat+'-'+vehicleId[element2.imei][0].lng+'-'+vehicleId[element2.imei][0].datetime);
  //                            send_email(element2.reminder_type_name, element2.des_email, element2.imei, vehicleId[element2.imei][0].lat, vehicleId[element2.imei][0].lng, notes, vehicleId[element2.imei][0].datetime);
                    }
                    if(element2.is_telegram==1){
                    console.log('telegram||'+element2.des_telegram+'||'+notes)
      //                        send_telegram(notes, element2, element2.des_telegram);
                    }
                  }
                });

        //loop hasil tabel, bandingkan dengan hasil mongo
                
                
            })
            .catch(error => {
              console.log(error.message)
            });

    }
    
    function getLastLocation (vehicleId, cb){
        //get semua last status dari mongo untuk tiap vehicle
                Object.keys(vehicleId).forEach(function(key) {
                    Lastlocation.find({ imei: key}, function (err, docs) {
                       vehicleId[key]=docs
                       console.log(docs);                        
                    });
                    console.log(key);
                }); 

                cb(vehicleId)    
    }
    
    function autoChecking(){ //auto check heart beat atm tiap interval tertentu
        let user_id = 176;
        setInterval(function () {
            let now = new Date();
                for (let key in arrayRecentDate) {
                    let diff = now-arrayRecentDate[key];
                    if(diff>900000){ //jika lebih dari 15 menit, maka dianggap offline
                        let notes = key+' is OFFLINE';
//                        live_notif(user_id,index,'ATM',notes);//type = ATM Asset
                        console.log(notes);
                        insertBeat(formatDate(now), key, 'OFFLINE');
                    } else {
                        console.log(key+': '+diff+' ms has passed since last update');
                        insertBeat(formatDate(now), key, 'ONLINE');
                    }
                }
        }, 60000); //60 detik
    }
//   autoChecking();

    function formatDate(date) {
        let monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];

        let day = date.getDate();
        let monthIndex = date.getMonth()+1;
        let year = date.getFullYear();

        let hour = date.getHours();
        let min = date.getMinutes();
        let sec = date.getSeconds();

        return year+'-'+addZero(monthIndex)+'-'+addZero(day)+' '+addZero(hour)+':'+addZero(min)+':'+addZero(sec);
    }

    function addZero(number)
    {
        if(number<10)
        {
            return '0'+number;
        }
        else
        {
            return number;
        }
    }

};