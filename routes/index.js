import path from "path";

module.exports = app => {
    // let cache = require(path.join(__dirname, '../cache/memcache'))(30);
    /**
     * @api {get} / API Status
     * @apiGroup Status
     * @apiVersion 1.0.1
     * @apiSuccess {String} status message api
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    {
     *      "status": "cctv monitoring service"
     *    }
     */
    app.get("/", (req, res) => {
        res.json({status : "reminder"});
    });
};