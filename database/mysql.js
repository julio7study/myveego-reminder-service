//for setting db
import Sequelize from "sequelize";
import path from "path";

let db = null;

module.exports = app => {
    if(!db){
        const config = app.libs.config;
        const sequelize = new Sequelize(config.database,
            config.username,
            config.password,
            config.params);

        db = {};
        db.sequelize = sequelize;
        db.Sequelize = Sequelize;

        //Model
        db.users = require("../models/users")(sequelize, Sequelize);
        db.notification = require("../models/notification")(sequelize, Sequelize);
    }

    return db;
};