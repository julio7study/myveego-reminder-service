import Mongoose from "mongoose";
import path from "path";

let db = null;
module.exports = app => {
    
    const cfg = app.libs.config.noSql;

class MongoDatabase
    {
        constructor()
        {
            this._connectAuth();
        }

        _connectHost()
        {
            Mongoose.set('useCreateIndex', true);
            Mongoose.connect(`mongodb://${cfg.host}:${cfg.port}/${cfg.database}`, 
                    { useNewUrlParser: true, keepAlive: 3000, user: cfg.user, pass: cfg.pass, auto_reconnect: true })
                .then(() => {
                    console.log("Database connected");
                })
                .catch(err => {
                    console.error(`Database connection error ${err}` );
                });
        }   

        _connectAuth()
        {
            let connection = `mongodb://${cfg.user}:${cfg.pass}@` +
                             `${cfg.host}:${cfg.port}/${cfg.database}?` +
                             `authSource=${cfg.dbadmin}`;
 
            Mongoose.set('useCreateIndex', true);
            Mongoose.connect(connection, { useNewUrlParser: true, keepAlive: 3000, auto_reconnect: true })
                .then(() => {
                    console.log("Database connected");
                })
                .catch(err => {
                    console.error(`Database connection error ${err}` );
                });
        }

        _connectUrl()
        {
            Mongoose.set('useCreateIndex', true);
            Mongoose.connect(`mongodb://${cfg.url}`, 
                    { useNewUrlParser: true, keepAlive: 3000, auto_reconnect: true })
                .then(() => {
                    console.log("Database connected");
                })
                .catch(err => {
                    console.error(`Database connection error ${err}` );
                });
        }
    }



    if(!db)
    {
        db = {};
        db.mongoose = new MongoDatabase();
        db.Mongoose = Mongoose;

        //append model
//        db.atmbeat = Mongoose.model('Atmbeat', require(path.join(__dirname, '../models/mongo/heartbeat'))(Mongoose), 'ATMBEAT');
        db.lastlocation = Mongoose.model('Lastlocation', require(path.join(__dirname, '../models/mongo/lastlocation'))(Mongoose), 'LASTLOCATION_view');
    }

    return db;
};